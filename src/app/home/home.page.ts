import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public isIOS: boolean;
  public isAndroid: boolean;
  public isDesktop: boolean;

  constructor(private platform: Platform, private http: HTTP, private httpClient: HttpClient) {
    this.isIOS = platform.is('ios');
    this.isDesktop = platform.is('desktop');
    this.isAndroid = platform.is('android');

    if (this.isAndroid || this.isIOS) {
      this.http.get('http://localhost:3000/test', {}, {})
        .then(data => {
          console.log(data);
        })
        .catch(error => {
          console.log(error);
        });
    } else if (this.isDesktop) {
      this.httpClient.get('http://localhost:3000/test').subscribe(res => {
        console.log(res);
      })
    }
  }

}
